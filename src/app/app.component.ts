import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AppService } from './app.service';
import { debounceTime, map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AppService]
})

export class AppComponent {

  searchTerm : FormControl = new FormControl();
  searchResult = [];
  text= "";
  people= "";
  title = 'app';
  hidden = true;
  image = [
    {
      thumbnailUrl:""
    },
    {
      thumbnailUrl:""
    }
  ] ;

  constructor( private service: AppService){
    this.searchTerm.valueChanges
        .pipe(debounceTime(400)) 
        .subscribe(data => {
            this.service.search_people(data).subscribe(response =>{
                this.searchResult = response
            })
        })      
  }

  setPepole(item){
    this.text = name;
    this.service.get_people(item.url).subscribe(response =>{
        this.people = response;
        this.service.get_image(item.name).subscribe(response =>{
            this.image = response
            this.hidden = false;
        })
    })
  }
 
  
}
