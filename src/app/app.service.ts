import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from "rxjs/operators";
import {RequestOptions, Request, RequestMethod, Headers} from '@angular/http';


let subscriptionKey = '61ed7fd297444f40afe859450c546e07';


@Injectable({
  providedIn: 'root'
})
export class AppService {
    
    urlPepole: string
    urlImage: string

    constructor(private http : Http){
        this.urlPepole  = 'https://swapi.co/api/people/'
        this.urlImage = 'https://api.cognitive.microsoft.com/bing/v7.0/images/search?count=2&q='
    }

    get_image(term){
        
        const headerss = new Headers();
        headerss.append('Ocp-Apim-Subscription-Key', subscriptionKey);
        headerss.append('Content-Type', 'application/json');
      
        const options = new RequestOptions({
            method : RequestMethod.Get,
            headers :headerss
        });

        return this.http.get(this.urlImage+term, options).pipe(map(res => {
            return res.json().value;
        }))
    }

    get_people(url){
        return this.http.get(url).pipe(map(res => {
            return res.json();
        }))
    }

    search_people(term){
        return this.http.get(this.urlPepole+'?search='+ term).pipe(map(res => {
            return res.json().results.map(item => {
                return item
            })
        }))
    }

    
}
